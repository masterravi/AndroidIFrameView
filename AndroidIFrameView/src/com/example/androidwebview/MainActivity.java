package com.example.androidwebview;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends Activity {
	private WebView webview;
	private static final String TAG = "Main";
	private ProgressDialog progressBar;
	private Boolean click = false;
	private String mime = "text/html";
	private String encoding = "utf-8";
	private String html = "<iframe id='a86d0156' name='a86d0156' src='http://kriteria.adswizz.com/www/delivery/afr.php?zoneid=1583&amp;target=_blank' framespacing='0' frameborder='no' scrolling='no'><a href='http://kriteria.adswizz.com/www/delivery/ck.php?n=a52d7cd3' target='_blank'><img src='http://kriteria.adswizz.com/www/delivery/avw.php?zoneid=1583&amp;n=a52d7cd3' border='0' alt='' /></a></iframe>";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		this.webview = (WebView) findViewById(R.id.web_view);

		webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webview.loadDataWithBaseURL(null, html, mime, encoding, null);
		progressBar = ProgressDialog.show(MainActivity.this, "WebView Example", "Loading...");
		webview.setWebViewClient(new WebViewClient() {

			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT).show();
				return false;
			}

			public void onPageFinished(WebView view, String url) {
				Log.i(TAG, "Finished loading URL: " + url);
				if (progressBar.isShowing()) {
					progressBar.dismiss();
				}
				click = true;
			}

			public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
				if (click == true) {
					Log.i("URL", url);
					Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT).show();
					final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
					startActivity(intent);
					webview.loadDataWithBaseURL(null, html, mime, encoding, null);
					click = false;
				}
				return super.shouldInterceptRequest(view, url);

			}

		});

	}

}
